from sqlalchemy.orm import Session

import models as models

import schemas as schemas

def get_itemsa(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Item).offset(skip).limit(limit).all()

def create_user_item(db: Session, item: schemas.ItemCreate):
    db_item = models.Item(**item.dict())
    db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return db_item

def update_user_item(id, db: Session, item: schemas.ItemCreate):
    db_item = db.query(models.Item).filter(models.Item.id == id).first()
    db_item.title = item.title
    db.commit()
    db.refresh(db_item)
    return db_item
    # return id