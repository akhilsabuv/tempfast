from sqlalchemy.schema import Column
from sqlalchemy.types import String, Integer
from database import Base
from sqlalchemy.orm import relationship


class Item(Base):
    __tablename__ = "items"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(16), index=True)