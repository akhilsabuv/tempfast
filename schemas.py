from pydantic import BaseModel

class ItemBase(BaseModel):
    title: str
    
class ItemCreate(ItemBase):
    pass