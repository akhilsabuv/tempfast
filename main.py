from fastapi import Depends, FastAPI, HTTPException
from typing import List
from database import SessionLocal, db_engine,Base
from sqlalchemy.orm import Session
import curd as curd
import schemas as schemas

Base.metadata.create_all(bind=db_engine)

app = FastAPI(title="FastAPI App", description="The first fast API App")

# //paths
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@app.get("/")
def read_users(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    users = curd.get_itemsa(db, skip=skip, limit=limit)
    return users


@app.post("/users")
def create_item_for_user(item: schemas.ItemCreate, db: Session = Depends(get_db)):
    return curd.create_user_item(db=db, item=item)

@app.put("/users/{id}")
def update_item_for_user(id,item: schemas.ItemCreate, db: Session = Depends(get_db)):
        return curd.update_user_item(db=db, item=item, id=id)
